var RevealBreadcrumb = window.RevealBreadcrumb || (function(){

    var options = Reveal.getConfig().breadcrumb || {};

    var headers = {};
    var HIDE = Symbol('hide');
    var KEEP = Symbol('keep');
    var currentIndex = null;

    var headlineBar = document.createElement('div');
    headlineBar.classList.add('breadcrumb');
    document.getElementsByClassName('reveal')[0].appendChild(headlineBar);

    Reveal.addEventListener( 'slidechanged', function( event ) {
        updateBreadcrumb( event.currentSlide );
    });

    function getCurrentTitle(currentSlide) {
        if (currentSlide.dataset['noBreadcrumb'] !== undefined &&
            currentSlide.parentElement.dataset['noBreadcrumb'] !== undefined) {
            return HIDE;
        }

        var titleElement = currentSlide.querySelector('h1, h2, h3, h4, h5');
        var indexToShow;
        if (!titleElement) {
            indexToShow = currentIndex;
        } else {
            currentIndex = parseInt(titleElement.tagName.slice(1), 10);
            headers[currentIndex] = titleElement;
            indexToShow = Math.min(Math.max(currentIndex - 1, 1), 5);

            if (currentIndex === indexToShow) {
                return HIDE;
            }
        }

        if (!headers[indexToShow]) {
            return HIDE;
        }

        return headers[indexToShow].innerHTML;
    }

    function updateBreadcrumb( currentSlide ) {
        var bs = document.querySelector('.breadcrumb');
        var title = getCurrentTitle(currentSlide);
        if (title === HIDE) {
            bs.style['opacity'] = 0;
        } else {
            if (title !== KEEP) {
                bs.innerHTML = title;
            }
            bs.style['opacity'] = 0.8;
        }
    }

    Reveal.addEventListener('ready', () => updateBreadcrumb(Reveal.getCurrentSlide()));
})();
