# Big Data Ethik: Datenethik
Arne Beer und Rafael Epplée


# What is Data Ethics?
Luciano Floridi and Mariarosaria Taddeo


## About
- Intro text for theme issue _The ethical impact of data science_
- We read several texts from this issue and we are going to read even more.
- Concerns the definition of data ethics

Notes:
- Other articles in this issue

- Data ethics baut auf Informations und Computer Ethik auf.
- Focus auf den Eigenschaften von Inhalten und 
- Durch die Komplexität der Daten und Verknüpfungen sollte es als MacroEthik angesehen werden
    -> Ein Framework um die Gesamtheit der verschiedenen betroffenen oder aufbauenden Ethik Kategorien zu vereinen und konsistent mit diesen zu arbeiten.


## Goals

- Foster the development and applications of data science
- Ensure respect of human rights
- -> Balance

Note:
- Es muss das development von data science gefördert werden, da klare Vorteile existieren
- Menschliche Rechte müssen eingehalten werden
- Schwierig balance zu finden


## Levels of Abstraction (LoA)
- New level of abstraction needed
- Change from _information_ to _data_

Note:
- Level of abstraction sind Werkzeug um Fokus auf bestimmte Aspekte zu lenken
- Perspektive ändern


### Former Levels of Abstraction
- Human-centric
- Computer-centric
- Information-centric

Note:
Human-centric: Während Verbreitung von Computern entstanden. Verantwortung von Designer und Nutzer im professionellen Kontext

Computer-centric: Betrachten von Computern als Werkzeug. Fokus ist Einfluss auf Beeinflussung von sozialer Dynamic und menschlichem Umfeld. 

Information-centric: Fokus auf Tatsächlichen inhalten. Information als Resultat von oder Zielobjekt für moralische Handlungen. Zyklus von Information wird betrachtet


## Data-centric LoA
- Latest phase of information revolution
- Focus on moral dimensions of data
- Privacy, anonymity, transparency, trust, responsibility

Note:
- Bevor es um information geht, muss zuerst das Sammeln, die Verwendung, Verwaltung und Analyse von auf diese Punkte bezogen werden


## Axes of Research
concerns of data ethics:
- Data
- Algorithms
- Correspondig practices

Note:
Data: Sammeln, analysieren und transformieren von Daten.

Algorithms: Komplexität und Autonomität von Algorithmen. Moralische Verantwortung und Verantwortlichkeit

Practices: Fortschritt, Programierung, Hacken, und professionelles coden.


## Challenges:
- Machine learning algorithms
- Design and auditing of algorithms
- Define responsibility and accountability

Note:
- Komplexität von ML verhindert einfaches Abschätzen von Konsequenzen
- Wer bestimmt Richtlinien und wer stellt ethische Korrektheit eines Algorithmus klar.
- Wer ist verantwortlich (z.B. innerhalb einer Firma)


# Big Data Ethics
- Andrej Zwitter
- Impact of actions can no longer be easily assessed 
- Knock-on effects

Note:
- Freigabe von Daten hat unbekannte Auswirkungen/Ausmaße
- Kontrolle von Datenbenutzung ist erschwert


## Traditional Ethics
- Who is responsible 
- How responsible can one be for his actions

Note:
- Basiert on Individualismus und freiem Willen


## Moral Agency
- Moral responsibility
- Three innate conditions:
    1. Causality
    2. Knowledge
    3. Choice

Note:
Der Grad von Verantwortlichkeit einer Person ist abhängig von:

- Causality: Man ist verantwortlich für seine Aktionen wenn diese die  Ursache für das Problem sind.
- Knowledge: Man kann beschuldigt werden, wenn man wissen über die möglichen Konsequenzen hatte/hätte haben sollen
- Choice: Man ist verantwortlich, falls man eine mögliche Alternative hatte, ohne sich selbst zu schaden.


## Four Qualities of Big Data
- More data than ever
- Big data is organic
- Big data is global
- Correlations versus causation

(human vs non-human data)

Note:
- More data than ever: 
    1. 2011: 5 billion GB every two days
    2. 2013: 5 billion GB every 10 min
    3. 2015: 5 billion GB every 10 seconds
- Organic: Unordentlich, aber stellt Realität besser dar. -> Bessere Abbildung der Realität
- Global: Je nach Datenset, wird der ganze Globus umfasst.
- Correlation versus causation: Korrelation wird von Big Data betont


Is this still viable?


## New Power Distributions
- Power changes to be distributed in a more networked fashion
- Retaining individual agency is one of main challenges
- Three groups:
    1. Big Data collectors
    2. Big Data utilizers
    3. Big Data generators

Note:
- Einfluss/Macht über andere 
- Neue Probleme/Dilemmata treten auf -> neue Versionen der standard moralischen Probleme
- Individuum muss Wissen und Möglichkeit haben zu handeln


### Roles of each group
- Collectors:
    1. Determine which data is collected and stored
    2. Government of access
- Utilizers:
    1. (re-)define the purpose for which data is used
- Generators:
    1. Natural actors, Artificial actors, pysical phenomena
    2. Create data

Note:
- storage: Wie lange, was wird gespeichert
- Utilizers: Z.B. Manipulieren von sozialen prozessen.
Erstellen von neuem Wissen/Erfindungen durch Verbinden von Data sets -> competetive advantage
- Generators: wissend, unwissend, freiwillig/unfreiwillig generierte Daten
    Künstliche Agenten, die als Ziel oder Nebenprodukt ihrer Tasks daten generieren


### Implications of a networked fashion
- Power of degree is relative to
    1. dependency between parties 
    2. capability of influencing another party


### Example 

<img src="/images/networked_power.png" height="700">

Note:
- Power ist abhängig von der Dichte des Netzwerks
- ein Netzwerk kann an sich eine Gesamtstärke haben


## Impact on Moral Agency
- Different view on individual agency
- Morally relevant actions are dependent on other actors
- Knock-on effects are possibly extreme in hyper-networked societies

Note:
- Es wird schwer die folgen der eigenen Handlung abzuschätzen. e.G. unfreiwillig gesammelte Daten.
- Die Tauglichkeit ethisch korrekt zu handeln hängt von anderen Acteuren ab.
- Man ist nicht mehr alleinig für seine Handlungen verantwortlich
- Facebook event Beispiel


## New Ethical Challenges
- Data generator is in disadvantage
- Internet of things (IoT)
- Global data leads to power imbalance
- Correlation suggests causation

Note:
- Data generator kann nicht abschätzen, welche Daten verwendet und gesammelt werden. -> Unwissenheit
- IoT fördert Datengenerierung noch mehr
- Firmen mit nötigem Know-How werden bevorzugt, da sie Wissen und Macht aus Daten anreichern können.
- Wir werden anfälliger Dinge zu glauben ohne sie zu verstehen. Selbsterfüllende Orakel.


### Privacy
- Personal information becomes transparent for several actors

Note:
- Mit den richtigen Tools und Zugängen ergeben sich sehr viele intime Informationen 


### Group Privacy
- Even de-individualized information can discriminate
- Data without information relevant to belongigness to a group doesn't exist
- Groups are becoming more transparent

Note:
- Anhand von verschiedenen Kriterien können Menschen immer Kategorisiert werden
- Bestimmte Eigenschaften von Gruppen werden ersichtlich
- Gezielte Anreize können eingesetzt werden, um ein bestimmtes Verhalten von einer Gruppe zu ermutigen oder davon abzuschrecken
- Example: fake twitter debates über politische Parteien
    2013: 61% visitors are bots. 20.5% Imitatoren, 0.5% spammer, 5% scraper


### Propensity (Tendenz)
- _Predictive policing_ (e.g. Minority Report)
- Detection of possible tendencies in people (e.g. domestic violence)
- Threshold?
- Random correlations might lead to wrong conclusions

Note:
- Analytics zeigen mögliche kriminelle hotspots
- Solche erkennung können eine Person schlecht dastehen lassen, obowohl keine Straftat bekangen wurde.
- Wann darf interveniert werden. Wer legt die Grenze fest
- Weiter problematisch in unsupervised learning mit selbstfindenden correlations.
- Je mehr daten existieren, desto wahrscheinlich sind zufällige Correlationen


### Research Ethics
- Are researchers allowed to use social media data?
- Research about groups can reveal uncomfortable truth
- Informed consent?

Note:
- Ethik hängt hinterher.
- Nutzung von öffentlichen Daten bleibt offene Frage
- Inwieweit dürfen derartige Forschungsergebnisse veröffentlicht werden
- Individuen sind überrascht plötzlich Teil einer Studie zu sein. Verstoß gegen Rechte? -> Major breach of research ethics


## Likely Developments
- Political campaign oberservers and alike will use new kinds of digital manipulation of public opinion
- Law enforcement needs to re-conceptualize individual guilt, probability and crime prevention
- States will redesign global strategies based on global data and algorithms


# The Ethics of Algorithms: Mapping the Debate
Brent Daniel Mittelstadt, Patrick Allo, Mariarosaria Taddeo, Sandra Wachter and Luciano Floridi


## Background
- Algorithms have increasing influence
- Many problems unsolved
- Provide a map to organise the debate

Notes: 
- Similar motivation to "What is data ethics", but with a narrower scope (algorithms).
- Algorithms change our perception
- ethical correctness is hard to verify


### Definition of "Algorithm"
A Mathematical construct

_or_

A particular implementation


"a finite, abstract, effective, compound control structure, imperatively given, accomplishing a given purpose under given provisions"

_compound_: zusammengesetzt, kompliziert, mehrschichtig

Notes:
audience questions: do you agree?
where are the limits of "given provisions"?
what about machine learning?


### Machine Learning
"Black Box" algorithms: data in, data out

Notes:
- Rules are automatically inferred from data
- Can adapt to new, never-seen data
- Some degree of autonomy
- Very difficult to predict or explain actions


## Map of the Ethics of Algorithms

<img src="/images/map_of_issues.png" height="700" width="1100">


### Inconclusive Evidence
- Correlation over Causality
- "Actionable Insights"
- Challenge: acknowledging the risk of being wrong

Notes:
- Actionable insight: sufficiently probable correlation, no causal connection needed


### Inscrutable Evidence
- Input data is opaque
- Explaining algorithms' decisions is very hard

Notes:
- data secret for commercial, security or privacy reasons
- conflict: commercial viability vs transparency
- besides being open, data must be understandable
- machine learning: high-dimensional data, changing and opaque decision rules
- traditional software: codebases too large 
- humans can explain their rationale behind a decision
- releasing a simplified explanation might help


### Misguided Evidence
Low-quality data leads to low-quality decisions

Notes:
- There is no objective data
- Developers and data add bias to algorithms


- Social bias
- Technical bias
- Emergent bias

Notes:
- social: intentional (search engine rankings) / unintentional (human-tagged data)
- technical: constraints, errors, algorithm design (alphabetical airline listing)
- emergent: outdated algorithms, changing users (medical recommendation systems biased toward what they know)


Algorithms' outputs are often _interpreted_

Notes:
Bias may come from humans, interpreting given correlations given their subjective context


### Unfair Outcomes
Even conclusive, scrutable, well-founded evidence may be _unfair_

Notes:
- e.g. discriminatory against a minority
- algorithms may discriminate using unobservable or incomprehensible characteristics
- further research: harm detection


Bias and Discrimination

Notes:
- bias: dimension of the decision-making
- discrimination: impact of a decision


Ideas
- Exclude sensitive attributes
- Distort training data
- Use anti-discrimination constraints
- Modify algorithm output

Notes:
- deferred attributes can still cause discrimination


### Transformative Effects

Algorithms can subtly change how we perceive the world

Notes:
- including social and political organization
- can lead to changes in behaviour


Supporting vs. Controlling

Notes:
- data selection skews humans' decisions
- presented data may reflect third party's interests -> reduced autonomy
- goal: show only relevant information
- relevance is subjective
- personalisation algorithms can reduce information diversity


Informational Privacy
- Identifiability / "personal" data
- Profiling / "anonymized", aggregated data

Notes:
- informational privacy: capacity to control information about oneself
- concerned only with identifiability
- explain how decisions were made
- allow people to opt-out of profiling
- profiling makes identifiability irrelevant -> further research
- how does privacy work with group characteristics? -> further research


### Traceability

- Detecting harm done by algorithms is hard
- So is assigning responsibility for the harm done

Note:
Inherited from the challenges of big data


- Accountability gap
- Moral machine agency

Notes:
- accountability gap between designer's control and algorithm's behaviour
- machine agency: open question
  - requires significant autonomy
  - denying agency makes designers responsible
  - neither is satisfactory: oversight is complex, algorithms are volatile
- question: what do you think?


Artificial Moral and Ethical Agents

Notes:
- constrain algorithms by ethical principles
- principles may be hard to formalise
- alternative: train algorithm to mimic human intuitions, principles, reasoning
- further research: distributed responsibility
- further research: high-quality auditing mechanisms


Hiding behind the machine

Notes:
- many people trust algorithms, assuming correctness by default
- people take less responsibility
- similar effect to bureaucracies, where people do otherwise unjustifiable things
- further research: how to minimise the inadvertent justification of harmful acts?


## Map of the Ethics of Algorithms

<img src="/images/map_of_issues.png" height="700" width="1100">


## GDPR

- Profiling must be _explained_ in plain language: rationale, significance and consequences
- Right to opt out or obtain human intervention

Notes:
- human intervention: allow people to contest algorithmic decisions


# Conclusion

- Data ethics is a young concept
- A basic layout of the field should improve future work


# Discussion

- Is algorithmic predictive policing justifiable?
- How to protect privacy in the wake of profiling? Would a limit on the amount of data used for profiling help?
